<head>
    <!-- Included to load an HTML file externally to a div -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <title>Choose Your Own Adventure</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css">
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Cagliostro' rel='stylesheet' type='text/css'>
</head>

<h1 class="mainheader">Daniel's Amazing <s>Choose</s> Create Your Own Adventure!</h1>

<body>
    <div class="entry"></div>
    <script>$(".entry").load("adventures/adventure0.html");</script>
</body>